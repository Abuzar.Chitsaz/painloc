import * as THREE from 'https://cdn.jsdelivr.net/npm/three@0.150.0/build/three.module.js';
import { OrbitControls } from 'https://cdn.jsdelivr.net/npm/three@0.150.0/examples/jsm/controls/OrbitControls.js';
import { GLTFLoader } from 'thttps://cdn.jsdelivr.net/npm/three@0.150.0/examples/jsm/loaders/GLTFLoader.js';
//import * as THREE from 'three';
//import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
//import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';

// npx vite

const scene = new THREE.Scene();
scene.background = new THREE.Color(0xf5f5f5);  // Définit le fond en blanc
const camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000); // FOV=45

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

const light = new THREE.AmbientLight(0xffffff, 0.75);
scene.add(light);
const directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
directionalLight.position.set(0, 4, 4);
scene.add(directionalLight);

const loader = new GLTFLoader();
let importedModel;
let originalColor = new THREE.Color(0xffffff); // Stocker la couleur originale ici

loader.load(
    '/Base%20Mesh%20sculpt%202.glb', // chemin vers le modèle   
    //'/ManBody_blender_directFromObj.glb',
    function (gltf) {   // fonction de rappel à exécuter une fois le modèle chargé
        importedModel = gltf.scene;
        importedModel.traverse(function (child) {
            if (child.isMesh) {
                // Créer un nouveau matériau MeshPhongMaterial
                child.material = new THREE.MeshPhongMaterial({
                    color: 0xffffff,
                    vertexColors: true
                });

                // Ajouter un attribut de couleur s'il n'existe pas
                if (!child.geometry.attributes.color) {
                    const colors = new Float32Array(child.geometry.attributes.position.count * 3);
                    for (let i = 0; i < colors.length; i += 3) {
                        colors[i] = originalColor.r; // R
                        colors[i + 1] = originalColor.g; // G
                        colors[i + 2] = originalColor.b; // B
                    }
                    child.geometry.setAttribute('color', new THREE.BufferAttribute(colors, 3));
                }

                child.material.needsUpdate = true;  // Mettre à jour le matériau s'il y en a une
            }
        });
        scene.add(importedModel);
    },
    undefined,  // fonction de rappel pendant le chargement (optionnelle)
    function (error) {  // fonction de rappel en cas d'erreur
        console.error(error);
    }
);

camera.position.z = 9;
camera.position.y = 3;

const controls = new OrbitControls(camera, renderer.domElement);
controls.target.set(0, 3, 0); // s'assurer que les contrôles pointent vers le centre de la scène
controls.update();
controls.enableDamping = true; // Effet optionnel qui rend les contrôles plus pondérés
controls.dampingFactor = 0.1;

const raycaster = new THREE.Raycaster();
const mouse = new THREE.Vector2();

let isPaintMode = false;
let isMouseDown = false; // Suivre l'état du bouton de la souris
let currentButton = null; // Suivre le bouton actuellement enfoncé (gauche ou droit)
let redFaces = new Map(); // Garder une trace des faces peintes en rouge et leurs couleurs originales
let redIntensity = 1; // Intensité du rouge, par défaut à 1 (plein rouge)

// Ajout d'un bouton pour activer/désactiver le mode peinture
const paintButton = document.createElement('button');
paintButton.textContent = 'Activer mode peinture';
paintButton.style.position = 'absolute';
paintButton.style.top = '10px';
paintButton.style.left = '10px';
document.body.appendChild(paintButton);

paintButton.addEventListener('click', () => {
    isPaintMode = !isPaintMode;
    controls.enabled = !isPaintMode;
    paintButton.textContent = isPaintMode ? 'Désactiver mode peinture' : 'Activer mode peinture';
});

// Ajout du slider pour ajuster l'intensité du rouge
const slider = document.createElement('input');
slider.type = 'range';
slider.min = '0';
slider.max = '1';
slider.step = '0.01';
slider.value = '1';
slider.style.position = 'absolute';
slider.style.top = '50px';
slider.style.left = '10px';
slider.style.width = '200px';
slider.style.background = 'linear-gradient(to right, white, red)';
document.body.appendChild(slider);

slider.addEventListener('input', (event) => {
    redIntensity = parseFloat(event.target.value);
});

function onMouseDown(event) {
    if (!isPaintMode) return;
    isMouseDown = true;
    currentButton = event.button; // Enregistrer quel bouton est enfoncé
    handlePainting(event); // Appliquer immédiatement la peinture ou l'effacement
}

function onMouseUp(event) {
    isMouseDown = false;
    currentButton = null; // Réinitialiser le bouton enfoncé
}

function onMouseMove(event) {
    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
    if (isPaintMode && isMouseDown) {
        handlePainting(event);
    }
}

function handlePainting(event) {
    raycaster.setFromCamera(mouse, camera);
    const intersects = raycaster.intersectObjects(scene.children, true);

    if (intersects.length > 0) {
        const intersect = intersects[0];
        const face = intersect.face;
        const geometry = intersect.object.geometry;
        const faceIndices = [face.a, face.b, face.c];

        if (currentButton === 2) { // Clic droit pour restaurer la couleur originale
            if (redFaces.has(face)) {
                const originalColors = redFaces.get(face);
                faceIndices.forEach((index, i) => {
                    geometry.attributes.color.setXYZ(index, ...originalColors[i]);
                });
                redFaces.delete(face);
            } else {
                faceIndices.forEach(index => {
                    geometry.attributes.color.setXYZ(index, originalColor.r, originalColor.g, originalColor.b);
                });
            }
        } else if (currentButton === 0) { // Clic gauche pour changer la couleur en rouge
            if (!redFaces.has(face)) {
                const originalColors = faceIndices.map(index => [
                    geometry.attributes.color.getX(index),
                    geometry.attributes.color.getY(index),
                    geometry.attributes.color.getZ(index)
                ]);
                redFaces.set(face, originalColors);
            }

            faceIndices.forEach(index => {
                geometry.attributes.color.setXYZ(index, 1, 1 - redIntensity, 1 - redIntensity);
            });
        }

        geometry.attributes.color.needsUpdate = true;
    }
}

// Ajout d'un écouteur d'événements pour le clic de souris
document.addEventListener('mousedown', onMouseDown, false);
document.addEventListener('mouseup', onMouseUp, false);
document.addEventListener('mousemove', onMouseMove, false);

// Ajout d'un écouteur d'événements pour désactiver le menu contextuel
document.addEventListener('contextmenu', event => event.preventDefault());

// Fonction pour effacer les coloriages actuels
function clearCurrentColoring() {
    if (importedModel) {
        importedModel.traverse(function (child) {
            if (child.isMesh && child.geometry.attributes.color) {
                const colors = child.geometry.attributes.color;
                for (let i = 0; i < colors.count; i++) {
                    colors.setXYZ(i, originalColor.r, originalColor.g, originalColor.b);
                }
                colors.needsUpdate = true;
            }
        });
    }
    redFaces.clear();
}

// Fonction pour sauvegarder les données dans un fichier JSON
function saveData() {
    const date = new Date();
    const data = {
        date: date.toISOString(),
        faces: Array.from(redFaces.entries()).map(([face, colors]) => ({
            face: [face.a, face.b, face.c],
            colors
        }))
    };
    const json = JSON.stringify(data, null, 2); // null et 2 pour indenter le JSON
    const blob = new Blob([json], { type: 'application/json' });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = `coloring_data_${date.toISOString()}.json`;
    a.click();
    URL.revokeObjectURL(url);
}

// Fonction pour charger les données depuis un fichier JSON
function loadData(file) {
    const reader = new FileReader();
    reader.onload = (event) => {
        const data = JSON.parse(event.target.result);
        clearCurrentColoring();
        data.faces.forEach(({ face, colors }) => {
            const geometry = importedModel.children[0].geometry; // Suppose qu'il n'y a qu'un seul enfant
            face.forEach((index, i) => {
                geometry.attributes.color.setXYZ(index, ...colors[i]);
            });
            geometry.attributes.color.needsUpdate = true;
        });
    };
    reader.readAsText(file);
}

// Ajout d'un bouton pour sauvegarder les données
const saveButton = document.createElement('button');
saveButton.textContent = 'Sauvegarder les données';
saveButton.style.position = 'absolute';
saveButton.style.top = '10px';
saveButton.style.left = '150px';
document.body.appendChild(saveButton);
saveButton.addEventListener('click', saveData);

// Ajout d'un input pour charger les données
const loadInput = document.createElement('input');
loadInput.type = 'file';
loadInput.style.position = 'absolute';
loadInput.style.top = '10px';
loadInput.style.left = '300px';
document.body.appendChild(loadInput);
loadInput.addEventListener('change', (event) => {
    const file = event.target.files[0];
    if (file) {
        loadData(file);
    }
});

function animate() {
    requestAnimationFrame(animate);
    controls.update();
    renderer.render(scene, camera);
}

animate();

function saveData() {
    const date = new Date();
    date.setHours(date.getHours() + 2); // Ajouter 2 heures à l'heure actuelle

    // Obtenir le modèle à partir de l'URL
    const modelType = getParameterByName('model');

    const data = {
        date: date.toISOString(),
        model: modelType, // Inclure le modèle (homme ou femme)
        faces: Array.from(redFaces.entries()).map(([face, colors]) => ({
            face: [face.a, face.b, face.c],
            colors
        }))
    };

    const json = JSON.stringify(data, null, 2);
    const blob = new Blob([json], { type: 'application/json' });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = `coloring_data_${date.toISOString()}.json`;
    a.click();
    URL.revokeObjectURL(url);
}
